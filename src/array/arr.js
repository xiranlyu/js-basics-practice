function joinArrays(arr1, arr2) {
  const arrayJoined = arr1.concat(arr2);
  return arrayJoined;
}

function checkAdult(arr, age) {
  const result = true;
  const adults = arr.filter(person => person > age);
  if (adults.length === arr.length) {
    return result;
  } else {
    return !result;
  }
}

function findAdult(arr, age) {
  let adult = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] >= age) {
      adult = arr[i];
      break;
    }
  }
  return adult;
}

function convertArrToStr(arr, str) {
  return arr.join(str);
}

function removeLastEle(arr) {
  arr.pop();
  return arr;
}

function addNewItem(arr, item) {
  arr.push(item);
  return arr;
}

function removeFirstItem(arr) {
  arr.shift();
  return arr;
}

function addNewItemToBeginArr(arr, item) {
  arr.unshift(item);
  return arr;
}

function reverseOrder(arr) {
  return arr.reverse();
}

function selectElements(arr, start, end) {
  return arr.slice(start, end);
}

function addItemsToArray(arr, index, howmany, item) {
  arr.splice(index, howmany, item);
  return arr;
}

function sortASC(arr) {
  return arr.sort(function(a, b) {
    return a - b;
  });
}

function sortDESC(arr) {
  return arr.sort(function(a, b) {
    return b - a;
  });
}

export {
  joinArrays,
  checkAdult,
  findAdult,
  convertArrToStr,
  removeLastEle,
  addNewItem,
  removeFirstItem,
  addNewItemToBeginArr,
  reverseOrder,
  selectElements,
  addItemsToArray,
  sortASC,
  sortDESC
};
