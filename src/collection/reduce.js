function getMaxNumber(collection) {
  return collection.reduce((previous, x) => Math.max(previous, x));
}

function isSameCollection(collection1, collection2) {
  const result = true;
  return collection1.reduce((previous, x, index) => collection2.length === collection1.length && x === collection2[index]
    ? result : !result);
}

function sum(collection) {
  return collection.reduce((sum, x) => sum + x);
}

function computeAverage(collection) {
  return collection.reduce((sum, x) => sum + x) / collection.length;
}

function lastEven(collection) {
  // return collection.reverse().find(x => x % 2 === 0);
  return collection.reduce((lastOne, thisOne) => thisOne % 2 === 0 ? thisOne : lastOne, 0);
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
