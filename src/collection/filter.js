function getAllEvens(collection) {
  return collection.filter(x => x % 2 === 0);
}

function getAllIncrementEvens(start, end) {
  const array = [];
  for (let i = start; i <= end; i++) {
    array.push(i);
  }
  return array.filter(x => x >= start && x <= end && x % 2 === 0);
}

function getIntersectionOfcollections(collection1, collection2) {
  return collection1.filter(x => collection2.includes(x));
}

function getUnionOfcollections(collection1, collection2) {
  const filteredArray = collection2.filter(x => !collection1.includes(x));
  return [...collection1, ...filteredArray];
}

function countItems(collection) {
  const counted = {};
  collection.forEach(element => {
    counted[element] = collection.filter(x => x === element).length;
  });
  return counted;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
