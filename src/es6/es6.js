function flatArray(arr) {
  return arr.flat();
}

function aggregateArray(arr) {
  return arr.map(x => [x * 2]);
}

function getEnumerableProperties(obj) {
  return Object.keys(obj);
}

function removeDuplicateItems(arr) {
  const newArray = [];
  arr.forEach(element => {
    if (!newArray.includes(element)) {
      newArray.push(element);
    }
  });
  return newArray;
}

function removeDuplicateChar(str) {
  const newArray = [];
  for (let i = 0; i < str.length; i++) {
    const char = str.charAt(i);
    if (!newArray.includes(char)) {
      newArray.push(char);
    }
  };
  return newArray.join('');
}

function addItemToSet(set, item) {
  return set.add(item);
}

function removeItemToSet(set, item) {
  set.delete(item);
  return set;
}

function countItems(arr) {
  const map = new Map();
  arr.forEach(element => {
    if (!map.has(element)) {
      map.set(element, 1);
    } else {
      let count = map.get(element);
      count++;
      map.set(element, count);
    }
  });
  return map;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
