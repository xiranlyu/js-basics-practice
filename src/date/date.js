function getDayOfMonth(dateStr) {
  const theDay = new Date(dateStr);
  return theDay.getDate();
}

function getDayOfWeek(dateStr) {
  const theDay = new Date(dateStr);
  return theDay.getDay();
}

function getYear(dateStr) {
  const theDay = new Date(dateStr);
  return theDay.getFullYear();
}

function getMonth(dateStr) {
  const theDay = new Date(dateStr);
  return theDay.getMonth();
}

function getMilliseconds(dateStr) {
  const theDay = new Date(dateStr);
  return theDay.getTime();
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
